<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$info = array(
	'name' => t('Блок «Поделиться» от Яндекса', __FILE__),
	'description' => t('Блок с иконками для добавления страниц сайта в различные социальные закладки.', __FILE__),
	'version' => '0.1',
	'author' => 'Bugo',
	'plugin_url' => 'https://gitlab.com/dragomano/yandex_share',
	'author_url' => 'https://gitlab.com/dragomano',
	'group' => 'template',
);

# end file