<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$info = array(
	'name' => t('Интеграция с SMF', __FILE__),
	'description' => t('C помощью этого плагина ваши комьюзеры будут автоматически авторизовываться на форуме.', __FILE__),
	'version' => '0.3',
	'author' => 'Bugo',
	'plugin_url' => 'https://gitlab.com/dragomano/smf_integration',
	'author_url' => 'https://gitlab.com/dragomano',
	'group' => 'template'
);

# end file