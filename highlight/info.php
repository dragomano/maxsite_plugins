<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$info = array(
	'name' => 'Highlight',
	'description' => t('Подсветка синтаксиса в блоках кода.', __FILE__),
	'version' => '0.2',
	'author' => 'Bugo',
	'plugin_url' => 'https://gitlab.com/dragomano/maxsite_plugins',
	'author_url' => 'https://gitlab.com/dragomano',
	'group' => 'template'
);

?>