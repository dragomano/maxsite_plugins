<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MaxSite CMS
 * Highlight plugin
 * Author: (c) Bugo
 * Author URL: https://gitlab.com/dragomano
 * Update URL: https://gitlab.com/dragomano/maxsite_plugins
 */

$lang['Подсветка синтаксиса в блоках кода.'] = 'This plugin highlights syntax in code examples.';
$lang['Стиль оформления'] = 'Style theme';
$lang['Выберите схему подсветки кода.'] = 'Choose preferred style theme for code blocks.';
$lang['Показывать номера строк'] = 'Show line numbers';
$lang['Для отображения номеров строк будет использоваться отдельный скрипт.'] = 'A separate script will be used to display line numbers.';
$lang['Настройки плагина Highlight'] = 'Highlight plugin settings';

?>