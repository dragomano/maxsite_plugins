# Highlight, плагин для MaxSite CMS
Плагин для подсветки синтаксиса на страницах вашего сайта.

![screenshot](https://user-images.githubusercontent.com/229402/104818259-56ad0000-5848-11eb-989e-c439a44c5444.png)

Используются скрипты Ивана Сагалаева [highlight.js](https://highlightjs.org) и Евгения Пакало [highlightjs-line-numbers.js](https://github.com/wcoder/highlightjs-line-numbers.js)

## Список изменений

### v0.2, 16.01.2021
* Обновлены стили и скрипты highlight.js (версия 10.5.0)
* Обновлены настройки
* Добавлена опция отображения нумерации строк

### v0.1, ~25.09.2011
* Первоначальный выпуск