<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MaxSite CMS
 * Highlight
 * Author: (c) Bugo
 * Plugin URL: https://gitlab.com/dragomano/maxsite_plugins
 */

function highlight_autoload($args = array())
{
	mso_hook_add('head_end', 'highlight_head_end');
	mso_hook_add('body_end', 'highlight_body_end');
	mso_hook_add('content', 'highlight_content');
}

function highlight_uninstall($args = array())
{
	mso_delete_option('plugin_highlight', 'plugins');

	return $args;
}

function highlight_mso_options()
{
	$styles = highlight_scan_files();

	mso_admin_plugin_options('plugin_highlight', 'plugins',
		array(
			'style' => array(
				'type' => 'select',
				'name' => t('Стиль оформления', __FILE__),
				'description' => t('Выберите схему подсветки кода.', __FILE__),
				'values' => $styles,
				'default' => 'default'
			),
			'line_numbers' => array(
				'type' => 'checkbox',
				'name' => t('Показывать номера строк', __FILE__),
				'description' => t('Для отображения номеров строк будет использоваться отдельный скрипт.', __FILE__),
				'default' => '0'
			)
		),
		t('Настройки плагина Highlight', __FILE__),
		t('<p>Плагин Highlight использует скрипт подсветки синтаксиса <a href="https://highlightjs.org" target="_blank" rel="noopener">highlight.js</a>, распространяемый на условиях <a href="https://github.com/highlightjs/highlight.js/blob/master/LICENSE" target="_blank" rel="noopener">лицензии BSD</a>.</p><p>По умолчанию поддерживается подсветка SQL, C++, Plain text, CSS, HTML, XML, PHP, Python, YAML, Markdown.</p><p>Для поддержки дополнительных языков скачайте файл <a href="https://highlightjs.org/download/" target="_blank" rel="noopener">highlight.pack.js</a>, отметив желаемые языки. Затем замените этим файлом соответствующий файл в директории <strong>application/maxsite/plugins/highlight</strong></p><br><p>Для вставки подсвечиваемого кода в записях используйте конструкцию вида <strong>[pre lang="язык"]ваш код[/pre]</strong>. Соответственно, не забудьте включить плагин <strong>bbcode</strong>.</p>', 'plugins')
	);
}

function highlight_head_end($args = array())
{
	$options = mso_get_option('plugin_highlight', 'plugins', array());

	if (!isset($options['style']) or !$options['style'])
		$options['style'] = 'default';

	$options['style'] = strtolower(str_replace(' ', '-', $options['style']));

	echo '
	<link rel="stylesheet" href="' . getinfo('plugins_url') . 'highlight/css/' . $options['style'] . '.css" media="screen">
	<style>pre {background: none !important; padding: 0 !important; white-space: normal !important; color: initial !important}</style>' . NR;

	return $args;
}

function highlight_body_end($args = array())
{
	echo '
	<script src="' . getinfo('plugins_url') . 'highlight/highlight.pack.js"></script>';

	$options = mso_get_option('plugin_highlight', 'plugins', array());

	if ($options['line_numbers']) {
		echo '
	<script src="' . getinfo('plugins_url') . 'highlight/highlightjs-line-numbers.min.js"></script>';
	}

	echo '
	<script>
		hljs.initHighlightingOnLoad();';

	if ($options['line_numbers']) {
		echo '
		hljs.initLineNumbersOnLoad();';
	}

	echo '
	</script>' . NR;

	return $args;

}

function highlight_content($text = '')
{
	$options = mso_get_option('plugin_highlight', 'plugins', array());

	if (!isset($options['style']) or !$options['style'])
		return $text;

	$text = str_ireplace('<pre', '<pre><code', $text);
	$text = str_ireplace('</pre>', '</code></pre>', $text);

	return $text;
}

function highlight_scan_files($cat = 'css')
{
	$CI = &get_instance();
	$CI->load->helper('directory');

	$path = getinfo('plugins_dir') . '/highlight/' . $cat;
	$files = directory_map($path, true);

	if (!$files)
		return '';

	$all_files = array();

	foreach ($files as $file) {
		if (@is_dir($path . $file) || strpos($file, '.png') !== false || strpos($file, '.jpg') !== false)
			continue;

		$file = str_replace('.css', '', $file);
		$all_files[] = ucwords(str_replace('-', ' ', $file));
	}

	sort($all_files);

	return implode('#', $all_files);
}

# end file