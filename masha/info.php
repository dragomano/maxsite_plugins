<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$info = array(
	'name' => 'Masha in MaxSite',
	'description' => t('Выделение интересных фрагментов текста с формированием специального url для доступа к этим пометкам.', __FILE__),
	'version' => '0.2',
	'author' => 'Bugo',
	'plugin_url' => 'https://gitlab.com/dragomano/masha',
	'author_url' => 'https://gitlab.com/dragomano',
	'group' => 'template'
);

?>