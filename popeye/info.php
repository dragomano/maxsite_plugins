<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$info = array(
	'name' => 'Popeye',
	'description' => t('Гибрид лайтбокса и слайдера.<br />Рекомендуемые размеры миниатюр: 240x173, полноразмерных изображений: 720x519.', __FILE__),
	'version' => '0.1',
	'author' => 'Bugo',
	'plugin_url' => 'https://gitlab.com/dragomano/popeye',
	'author_url' => 'https://gitlab.com/dragomano',
	'group' => 'template'
);

# end file