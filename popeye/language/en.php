<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Language for MaxSite CMS
 * Popeye plugin
 * Author: (c) Bugo
 * Author URL: https://gitlab.com/dragomano
 * Update URL: https://gitlab.com/dragomano/popeye
 * Script URL: http://dev.herr-schuessler.de/jquery/popeye/index.html
 */

$lang['Гибрид лайтбокса и слайдера.'] = 'An inline lightbox alternative with slideshow.';
$lang['Назад'] = 'Back';
$lang['Предыдущее изображение'] = 'Previous image';
$lang['Увеличить'] = 'Enlarge';
$lang['Закрыть'] = 'Close';
$lang['Дальше'] = 'Next';
$lang['Следующее изображение'] = 'Next Image';
$lang['Скриншот'] = 'Image';
$lang['из'] = 'of';

?>