<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * For MaxSite CMS
 * Popeye plugin
 * Author: (c) Bugo
 * Plugin URL: https://gitlab.com/dragomano/popeye
 */

function popeye_autoload()
{
	mso_hook_add('head', 'popeye_head');
	mso_hook_add('content_out', 'popeye_content');
}

function popeye_head($args = array())
{
	echo '
	<link rel="stylesheet" href="' . getinfo('plugins_url') . 'popeye/css/jquery.popeye.css" media="screen" />
	<script src="' . getinfo('plugins_url') . 'popeye/js/jquery.popeye-2.1.min.js"></script>
	<script>jQuery(document).ready(function($){$("#screenshots").popeye();});</script>' . NR;
}

function popeye_content($text = '')
{
   $preg = array(
		'~<p>\[gal=(.*?)\[\/gal\]</p>~si' => '[gal=$1[/gal]',
		'~<p>\[gallery(.*?)\](\s)*</p>~si' => '[gallery$1]',
		'~<p>\[\/gallery\](\s)*</p>~si' => '[/gallery]',
		'~<p>\[gallery(.*?)\](\s)*~si' => '[gallery$1]',
		'~\[\/gallery\](\s)*</p>~si' => '[/gallery]',
		'~\[gallery\](.*?)\[\/gallery\]~si' => '
		<div id="screenshots" class="ppy ppy-active">
			<div class="ppy-imglist">$1</div>
			<div class="ppy-outer">
				<div class="ppy-stagewrap">
					<div class="ppy-stage">
						<div class="ppy-nav">
							<a title="' . t('Назад', __FILE__) . '" class="ppy-prev">' . t('Предыдущее изображение', __FILE__) . '</a>
							<a title="' . t('Увеличить', __FILE__) . '" class="ppy-switch-enlarge">' . t('Увеличить', __FILE__) . '</a>
							<a title="' . t('Закрыть', __FILE__) . '" class="ppy-switch-compact ppy-hidden">' . t('Закрыть', __FILE__) . '</a>
							<a title="' . t('Дальше', __FILE__) . '" class="ppy-next">' . t('Следующее изображение', __FILE__) . '</a>
						</div>
					</div>
				</div>
			</div>
			<div class="ppy-caption">
				<div class="ppy-counter">' . t('Скриншот', __FILE__) . ' <strong class="ppy-current"></strong> ' . t('из', __FILE__) . ' <strong class="ppy-total"></strong></div>
				<span class="ppy-text"></span>
			</div>
		</div>',
		'~\[gal=(.[^\s]*?) (.*?)\](.*?)\[\/gal\]~si' => '<figure><a href="$3"><img src="$1" alt=""></a><figcaption>$2</figcaption></figure>',
		'~\[gal=(.*?)\](.*?)\[\/gal\]~si' => '<figure><a href="$2"><img src="$1" alt=""></a></figure>',
		'~\[image\](.*?)\[\/image\]~si' => '<figure><a href="$1"><img src="$1" alt=""></a></figure>',
		'~\[image=(.[^\s]*?) (.*?)\](.*?)\[\/image\]~si' => '<figure><a href="$3"><img src="$1" alt=""></a><figcaption>$2</figcaption></figure>',
		'~\[image=(.[^ ]*?)\](.*?)\[\/image\]~si' => '<figure><a href="$2"><img src="$1" alt=""></a></figure>',
		'~\[image\((.[^\s]*?)\)=(.[^\s]*?) (.*?)\](.*?)\[\/image\]~si' => '<figure><a href="$4" title="3"><img src="$2" alt="" class="$1"></a><figcaption>$3</figcaption></figure>',
		'~\[image\((.[^ ]*?)\)=(.[^ ]*?)\](.*?)\[\/image\]~si' => '<figure><a href="$3"><img src="$2" alt="" class="$1"></a></figure>',
		'~\[image\((.[^ ]*?)\)\](.*?)\[\/image\]~si' => '<figure><a href="$2"><img src="$2" alt="" class="$1"></a></figure>',
		'~\[galname\](.*?)\[\/galname\]~si' => '<div>$1</div>',
	);

	return preg_replace(array_keys($preg), array_values($preg), $text);
}

# end file