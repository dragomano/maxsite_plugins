<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$info = array(
	'name' => t('Дай рубль', __FILE__),
	'description' => t('Виджет Яндекс.Денег для благотворителей', __FILE__),
	'version' => '0.2',
	'author' => 'Bugo',
	'plugin_url' => 'https://gitlab.com/dragomano/collector',
	'author_url' => 'https://gitlab.com/dragomano',
	'group' => 'template'
);

# end file