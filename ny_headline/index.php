<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * For MaxSite CMS
 * NY Headline plugin
 * Author: (c) Bugo
 * Plugin URL: https://gitlab.com/dragomano/ny_headline
 */

function ny_headline_autoload()
{
	mso_hook_add('head', 'ny_headline_head');
	mso_hook_add('body_start', 'ny_headline_content');
}

function ny_headline_head()
{
	echo '
	<link rel="stylesheet" href="' . getinfo('plugins_url') . 'ny_headline/css/ny_headline.css" media="screen" />
	<script src="' . getinfo('plugins_url') . 'ny_headline/js/swfobject.min.js"></script>
	<script src="' . getinfo('plugins_url') . 'ny_headline/js/newyear.js"></script>' . NR;
}

function ny_headline_content()
{
	echo '
	<div class="b-page_newyear">
		<div class="b-page__content">
			<i class="b-head-decor">
				<i class="b-head-decor__inner b-head-decor__inner_n1">
					<span class="b-ball b-ball_n1 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n2 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n3 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n4 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n5 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n6 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n7 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n8 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n9 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i1"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i2"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i3"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i4"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i5"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i6"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
				</i>
				<i class="b-head-decor__inner b-head-decor__inner_n2">
					<span class="b-ball b-ball_n1 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n2 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n3 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n4 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n5 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n6 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n7 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n8 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n9 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i1"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i2"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i3"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i4"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i5"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i6"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
				</i>
				<i class="b-head-decor__inner b-head-decor__inner_n3">
					<span class="b-ball b-ball_n1 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n2 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n3 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n4 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n5 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n6 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n7 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n8 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n9 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i1"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i2"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i3"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i4"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i5"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i6"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
				</i>
				<i class="b-head-decor__inner b-head-decor__inner_n4">
					<span class="b-ball b-ball_n1 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n2 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n3 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n4 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n5 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n6 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n7 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n8 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n9 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i1"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i2"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i3"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i4"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i5"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i6"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
				</i>
				<i class="b-head-decor__inner b-head-decor__inner_n5">
					<span class="b-ball b-ball_n1 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n2 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n3 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n4 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n5 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n6 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n7 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n8 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n9 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i1"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i2"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i3"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i4"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i5"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i6"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
				</i>
				<i class="b-head-decor__inner b-head-decor__inner_n6">
					<span class="b-ball b-ball_n1 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n2 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n3 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n4 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n5 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n6 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n7 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n8 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n9 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i1"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i2"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i3"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i4"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i5"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i6"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
				</i>
				<i class="b-head-decor__inner b-head-decor__inner_n7">
					<span class="b-ball b-ball_n1 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n2 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n3 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n4 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n5 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n6 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n7 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n8 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_n9 b-ball_bounce"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i1"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i2"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i3"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i4"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i5"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
					<span class="b-ball b-ball_i6"><span class="b-ball__right"></span><span class="b-ball__i"></span></span>
				</i>
			</i>
		</div>
	</div>
	<script>var ny_swf = "' . getinfo('plugins_url') . 'ny_headline/ny2012.swf";</script>' . NR;
}

# end file