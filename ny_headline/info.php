<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$info = array(
	'name' => t('Новогодняя шапка', __FILE__),
	'description' => t('Красивая праздничная шапка, скромно позаимствованная у Яндекс.Почты.', __FILE__),
	'version' => '0.1',
	'author' => 'Bugo',
	'plugin_url' => 'https://gitlab.com/dragomano/ny_headline',
	'author_url' => 'https://gitlab.com/dragomano',
	'group' => 'template'
);

# end file