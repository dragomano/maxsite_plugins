<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$info = array(
	'name' => 'Lighter',
	'description' => t('Подсветка синтаксиса (JS, PHP, CSS, HTML, Ruby, Shell, SQL, Markdown).', __FILE__),
	'version' => '0.2',
	'author' => 'Bugo',
	'plugin_url' => 'https://gitlab.com/dragomano/lighter',
	'author_url' => 'https://gitlab.com/dragomano',
	'group' => 'template'
);

# end file